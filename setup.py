#! /usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2010-2021 OneLogin, Inc.
# MIT License

from sys import path as sys_path
from os import path
from setuptools import setup, find_packages

# The directory containing this file
HERE = path.abspath(path.dirname(__file__))
if HERE not in sys_path:
    sys_path.insert(0, HERE)

from truefilm import __version__

# The text of the README file
with open(path.join(HERE, 'README.md')) as f:
    README = f.read()

# The requirements
with open(path.join(HERE, 'requirements.txt')) as f:
    REQUIREMENTS = f.read().splitlines()

PACKAGE_NAME = 'truefilm'

# This call to setup() does all the work
# python setup.py sdist --formats=zip bdist_wheel && pip install ./dist/PACKAGE_NAME-1.0.0.zip
setup(
    name=PACKAGE_NAME,
    version=__version__,
    description='Provide the ratio of budget to revenue for movie in the IMDB dataset along with the corresponding Wipedia page',
    long_description=README,
    long_description_content_type='text/markdown',
    keywords='python package test',
    url='https://github.com/ome9ax',
    author='Eddy Delta',
    author_email='edrdelta@gmail.com',
    license='MIT',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha', # NEXT : 3 - Alpha, 4 - Beta, 5 - Production/Stable, >>> 6 - Mature
        'Intended Audience :: Developers',
        'Intended Audience :: System Administrators',
        'Topic :: Software Development :: Build Tools',
        'Operating System :: OS Independent',
        'License :: OSI Approved :: MIT License',
        # 'Programming Language :: Python :: 3',
        # 'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
    ],
    python_requires='>=3.8',
    include_package_data=True,
    package_dir={
        '': PACKAGE_NAME,
    },
    install_requires=REQUIREMENTS,
    extras_require={
        'test': (
            'coverage>=4.5.2',
            'pylint==1.9.4',
            'flake8>=3.6.0',
            'coveralls==1.5.1',
        ),
    },
    entry_points={'console_scripts': ['ome9ax=%s.__main__:main' % PACKAGE_NAME]},
)
