[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![Pipeline Status](https://gitlab.com/omegax/truefilm/badges/main/pipeline.svg)](https://gitlab.com/omegax/truefilm/-/commits/main)
[![Build Status](https://gitlab.com/omegax/truefilm/badges/main/build.svg)](https://gitlab.com/omegax/truefilm/-/commits/main) 
[![Coverage Report](https://gitlab.com/omegax/truefilm/badges/main/coverage.svg)](https://gitlab.com/omegax/truefilm/-/commits/main)
<!-- https://gitlab.com/%{project_path}/-/commits/%{default_branch} -->
<!-- https://gitlab.com/%{project_path}/badges/%{default_branch}/build.svg -->

# True Film

## Introduction

TrueFiIm is a film investment company - they fund new projects, with the goal of taking a share of any profits. In the past these decisions were made on gut feeling, but as the industry becomes more and more competitive they would like to become more data driven. They believe that by understanding which films have performed well in the past, they can make better decisions.

## Challenge

You have been tasked with combining multiple data sources to allow them to do the following:
	• Perform queries to understand the top performing genres and production companies
	• Be easily able to navigate to the corresponding wikipedia page for each film, in order to read deeper into interesting films

Deliverable
- [ ] For each film, calculate the ratio of budget to revenue;
- [ ] Match each movie in the IMDB dataset with its corresponding Wikipedia page;
- [ ] Load the top 1000 movies (with the highest ratio) into a Postgres database, including the following for each movie: Title, budget, year, revenue, rating, ratio, production company, link to Wikipedia page, the Wikipedia abstract
- [ ] Make the process reproducible and automated.

## How to run it, and how to query the data

run it from the root folder with the following command
```python
python -m truefilm
```
More command option, ask for `--help`
```python
python -m truefilm -h
```

Optional installation
```python
python setup.py sdist --formats=zip bdist_wheel && pip install ./dist/truefilm-1.0.0.zip
```

## Why you've chosen each tool

For Postgres I would recommand this manual to get it installed on your env
[PostgreSQL Installation](https://sodocumentation.net/postgresql)

I also relied on
- `lxml` as is proved to be the fastest way to parse the Wikipedia abstract xml dump
- `python-Levenshtein` to calculate the `edit distance` between the IMDB movie title and the wikipedia page title. Although not perfect, it's been to closest match that I can get
- `numpy` to broadcast the calculation across a movie title and all the wikipedia cleaned titles. It's a brute force approach, but the broadcast distributed by the future multiprocessing made it a viable and scalable option.
- `pandas` and `sqlalchemy` to store the dataframe in the database on both PostgreSQL, SQLite, and MonetDB with a minimal code change

## Any algorithmic choices

I choose to leverage numpy broadcasting through the `vectorize` function to speedup the edit distance calculation over the movie titles selection. It seems to be an acceptable solution as the research of the best match is done in a simple line a code.
```python
take(WIKI_TITLES['ids'], argmin(V_DISTANCE(values[:, newaxis], WIKI_TITLES['values']), axis=1))
```

I took some time to dig a bit as I couldn't get the levenshtein distance fully use all my CPU/GPU power. I found out a few new approach for the edit distance calculation, which are based on `openmp` to better leverage modern chipset architecture. This could be a lead I'd like to explore.

This became a common challenge in genomic and other fields where getting the edit distance between patterns could make a difference.

To be continued...

## How you would test for correctness

```python
pytest test -p no:cacheprovider -n 4 -v
```

## Data sources

- [Wikipedia - enwiki-latest-abstract.xml.gz](https://dumps.wikimedia.org/enwiki/latest/enwiki-latest-abstract.xml.gz) (722MB)
- [Kaggle - movies_metadata.csv](https://www.kaggle.com/rounakbanik/the-movies-dataset/version/7#movies_metadata.csv) (228MB)

## TODO

CI/CD
- [ ] Configure build
- [ ] Configure coverage

Resources
- https://stackoverflow.com/questions/40891054/is-there-a-way-to-show-build-status-code-coverage-per-branch
- https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html
- https://pr-agite.univ-lille2.fr/help/user/project/pipelines/settings.md
- https://docs.gitlab.com/ee/topics/autodevops/index.html
