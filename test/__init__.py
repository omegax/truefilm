
from os import path, mkdir, remove
import pathlib
from copy import copy

import zipfile
from io import TextIOWrapper
from csv import DictReader

import gzip

from lxml import etree
from lxml.etree import iterparse

from truefilm.rating import benchmark

def build_test_wiki_data(file_path: str, source_dir:str):

    # ids = {2846844, 3691113, 3159068, 2313209,  693637, 1399154, 4887662,
    #     5287342, 2201113, 3910831,  206775,   99242, 5121280, 6058032,
    #     5233136,  145185, 1959422,   93676,  242085,  819502, 2391812,
    #     4717707, 3038950, 4991368,  301238, 2195717, 1108224,   14753,
    #     169155, 5950413}
    urls = {
        'https://en.wikipedia.org/wiki/Paranormal_Activity',
        'https://en.wikipedia.org/wiki/Paranormal_Activity_4',
        'https://en.wikipedia.org/wiki/Paranormal_Activity_2',
        'https://en.wikipedia.org/wiki/The_Blair_Witch_Project',
        'https://en.wikipedia.org/wiki/The_Bogus_Witch_Project',
        'https://en.wikipedia.org/wiki/The_Playa_Rich_Project',
        'https://en.wikipedia.org/wiki/The_Tiger:_An_Old_Hunter%27s_Tale',
        'https://en.wikipedia.org/wiki/Whispers:_An_Elephant%27s_Tale',
        'https://en.wikipedia.org/wiki/Dragon:_the_Old_Potter%27s_Tale',
        'https://en.wikipedia.org/wiki/Eraserhead',
        'https://en.wikipedia.org/wiki/Eraserheads',
        'https://en.wikipedia.org/wiki/Braehead',
        'https://en.wikipedia.org/wiki/The_Prey_of_the_Dragon',
        'https://en.wikipedia.org/wiki/The_Way_of_the_Dog',
        'https://en.wikipedia.org/wiki/The_Sons_of_the_Dragon',
        'https://en.wikipedia.org/wiki/Pink_Flamingos',
        'https://en.wikipedia.org/wiki/Hank_Flamingo',
        'https://en.wikipedia.org/wiki/The_Flamingos',
        'https://en.wikipedia.org/wiki/Super_Size_Me',
        'https://en.wikipedia.org/wiki/Supersize_She',
        'https://en.wikipedia.org/wiki/Super_Slim_Me',
        'https://en.wikipedia.org/wiki/The_Gallows',
        'https://en.wikipedia.org/wiki/The_Mallows',
        'https://en.wikipedia.org/wiki/The_Shallows',
        'https://en.wikipedia.org/wiki/Open_Water_(film)',
        'https://en.wikipedia.org/wiki/Open_Water',
        'https://en.wikipedia.org/wiki/Pentwater',
        'https://en.wikipedia.org/wiki/The_Texas_Chain_Saw_Massacre',
        'https://en.wikipedia.org/wiki/The_Texas_Chainsaw_Massacre_2',
        'https://en.wikipedia.org/wiki/The_German_Chainsaw_Massacre'
    }

    testfile_path = path.join(source_dir, 'wiki-data.xml.gz')
    if path.exists(testfile_path):
        remove(testfile_path)

    with gzip.open(file_path, 'r') as xml_file, \
         gzip.open(testfile_path, 'w') as output_file:

        root = etree.Element('feed')
        context = etree.iterparse(xml_file, events=('end',), tag='doc', huge_tree=True)
        for event, element in context:
            if element.findtext('url') in urls:
                # etree.SubElement(root, element.tag, element.attrib)
                # root.append(etree.fromstring(etree.tostring(element)))
                # str(etree.tostring(element, encoding='utf-8', pretty_print=True), 'utf-8')
                root.append(copy(element))
            # Free memory.
            element.clear()
            while element.getprevious() is not None:
                del element.getparent()[0]
        # output_file.truncate()
        output_file.write(etree.tostring(root, encoding='utf-8', pretty_print = True))
        # root.write(output_file)

from csv import DictReader

def build_test_movie_data(file_path: str, source_dir:str):

    testfile_path = path.join(source_dir, 'archive.zip')

    columns = ['adult', 'belongs_to_collection', 'budget', 'genres', 'homepage', 'id', 'imdb_id', 'original_language', 'original_title', 'overview', 'popularity', 'poster_path', 'production_companies', 'production_countries', 'release_date', 'revenue', 'runtime', 'spoken_languages', 'status', 'tagline', 'title', 'video', 'vote_average', 'vote_count']
    ids = {'tt1179904', 'tt0185937', 'tt5066556', 'tt0074486', 'tt0068935', 'tt0069089', 'tt0390521', 'tt2309260', 'tt0374102', 'tt0072271'}

    with zipfile.ZipFile(file_path) as zf, \
         zipfile.ZipFile(testfile_path, 'w') as tzf:
        with TextIOWrapper(zf.open('movies_metadata.csv', 'r'), encoding='utf-8') as metadata_file:
            headers = metadata_file.readline()
            content = ''.join(
                line
                for line in metadata_file.readlines()
                if next(DictReader([','.join(columns), line]))['imdb_id'] in ids
            )
            tzf.writestr('movies_metadata.csv', headers + content)

def build_test_data(imdbfile_path: str, wikipediafile_path: str, source_dir:str = pathlib.Path().absolute()):
    data_dir = path.join(source_dir, 'test', 'data')

    if not path.isdir(data_dir):
        mkdir(data_dir)

    benchmark(build_test_movie_data)(imdbfile_path, data_dir)
    benchmark(build_test_wiki_data)(wikipediafile_path, data_dir)
