
# Third party imports
from pytest import fixture, mark
from numpy.testing import assert_array_equal

from truefilm.rating import (
    path,
    pathlib,
    pd,
    load_imdb,
    load_wiki,
    get_movie_rating,
)


@fixture
def args_data():
    '''Use custom parameters set'''

    source_dir = path.join(pathlib.Path().absolute(), 'test', 'data')

    return {
        'imdbfile_path': path.join(source_dir, 'archive.zip'),
        'wikipediafile_path': path.join(source_dir, 'wiki-data.xml.gz'),
        'max_movies': 10,
        'max_processes': 1,
        'source_dir': source_dir,
    }


@fixture
def roi_data():
    '''Output data'''

    columns = ['imdb_id', 'release_date', 'title', 'original_title', 'movie_id', 'release_year', 'production_companies', 'rating', 'revenue', 'budget', 'roi_ratio']
    values = [
        ['tt0069089', '1972-03-12', 'Pink Flamingos', 'Pink Flamingos', 0, 1972, 'Dreamland Productions', 6.2, 6000000.0, 12000.0, 500.0],
        ['tt0072271', '1974-10-01', 'The Texas Chain Saw Massacre', 'The Texas Chain Saw Massacre', 1, 1974, 'New Line Cinema, Vortex', 7.1, 30859000.0, 85000.0, 363.0470588235294],
        ['tt0185937', '1999-07-14', 'The Blair Witch Project', 'The Blair Witch Project', 2, 1999, 'Artisan Entertainment, Haxan Films', 6.3, 248000000.0, 60000.0, 4133.333333333333],
        ['tt0074486', '1977-03-19', 'Eraserhead', 'Eraserhead', 3, 1977, 'American Film Institute (AFI), Libra Films', 7.5, 7000000.0, 10000.0, 700.0],
        ['tt0068935', '1972-06-01', 'The Way of the Dragon', '猛龍過江', 4, 1972, 'Golden Harvest Company, Concord Productions Inc.', 7.4, 85000000.0, 130000.0, 653.8461538461538],
        ['tt0390521', '2004-01-17', 'Super Size Me', 'Super Size Me', 5, 2004, 'Kathbur Pictures', 6.6, 28575078.0, 65000.0, 439.6165846153846],
        ['tt0374102', '2004-08-06', 'Open Water', 'Open Water', 6, 2004, 'Plunge Pictures LLC', 5.3, 54667954.0, 130000.0, 420.52272307692306],
        ['tt1179904', '2007-09-14', 'Paranormal Activity', 'Paranormal Activity', 7, 2007, 'Blumhouse Productions, Solana Films', 5.9, 193355800.0, 15000.0, 12890.386666666667],
        ['tt2309260', '2015-07-10', 'The Gallows', 'The Gallows', 8, 2015, 'New Line Cinema, Blumhouse Productions, Management 360, Tremendum Pictures', 4.9, 42664410.0, 100000.0, 426.6441],
        ['tt5066556', '2015-12-16', "The Tiger: An Old Hunter's Tale", '대호', 9, 2015, 'Next Entertainment World, Sanai Pictures', 7.5, 11083449.0, 5000.0, 2216.6898]
    ]

    return pd.DataFrame(values, columns=columns).sort_values(by='roi_ratio', ascending=False)


@fixture
def wiki_data():
    '''Output data'''

    columns = ['page_id', 'page_title', 'page_movie_title', 'page_url', 'page_abstract']
    values = [
        [0, 'Wikipedia: The Texas Chain Saw Massacre', 'the texas chain saw massacre', 'https://en.wikipedia.org/wiki/The_Texas_Chain_Saw_Massacre', '| story          ='],
        [1, 'Wikipedia: The Flamingos', 'the flamingos', 'https://en.wikipedia.org/wiki/The_Flamingos', '| current_members  = The FlamingosThe United States Patent and Trademark Office, US Registration Number:3751169 featuring Terry JohnsonTerry "Buzzy" JohnsonStarling NewsomeStan PrinstonTheresa Trigg - Musical Director'],
        [2, 'Wikipedia: Braehead', 'braehead', 'https://en.wikipedia.org/wiki/Braehead', '}}'],
        [3, 'Wikipedia: Pink Flamingos', 'pink flamingos', 'https://en.wikipedia.org/wiki/Pink_Flamingos', '| narrator       = John Waters'],
        [4, 'Wikipedia: The Texas Chainsaw Massacre 2', 'the texas chainsaw massacre 2', 'https://en.wikipedia.org/wiki/The_Texas_Chainsaw_Massacre_2', '| starring       = Dennis Hopper'],
        [5, 'Wikipedia: Eraserheads', 'eraserheads', 'https://en.wikipedia.org/wiki/Eraserheads', '| years_active     ='],
        [6, 'Wikipedia: Super Size Me', 'super size me', 'https://en.wikipedia.org/wiki/Super_Size_Me', 'Supersize Me (Beavis and Butt-Head)}}'],
        [7, 'Wikipedia: Open Water (film)', 'open water', 'https://en.wikipedia.org/wiki/Open_Water_(film)', '| runtime        = 79 minutes'],
        [8, 'Wikipedia: The Bogus Witch Project', 'the bogus witch project', 'https://en.wikipedia.org/wiki/The_Bogus_Witch_Project', '| runtime = 85 minutes'],
        [9, 'Wikipedia: Supersize She', 'supersize she', 'https://en.wikipedia.org/wiki/Supersize_She', '| distributor    = RDF RightsChannel 5TLCNine NetworkDiscovery EuropeRDF gets first look at Special Edition'],
        [10, 'Wikipedia: Pentwater', 'pentwater', 'https://en.wikipedia.org/wiki/Pentwater', 'Pentwater may refer to a community in the United States:'],
        [11, 'Wikipedia: The Playa Rich Project', 'the playa rich project', 'https://en.wikipedia.org/wiki/The_Playa_Rich_Project', '}}'],
        [12, 'Wikipedia: Hank Flamingo', 'hank flamingo', 'https://en.wikipedia.org/wiki/Hank_Flamingo', 'Hank Flamingo was an American country music band founded in the late 1980s. Its membership comprised Trent Summar (lead vocals), Philip Wallace (guitar),  Eddie Grigg (guitar, vocals), Ben Northern (bass guitar, vocals), Stuart E.'],
        [13, 'Wikipedia: Open Water', 'open water', 'https://en.wikipedia.org/wiki/Open_Water', 'Open water may refer to:'],
        [14, "Wikipedia: Dragon: the Old Potter's Tale", "dragon: the old potter's tale", 'https://en.wikipedia.org/wiki/Dragon:_the_Old_Potter%27s_Tale', 'is a short story by Ryūnosuke Akutagawa. It was first published in a 1919 collection of Akutagawa short stories, Akutagawa Ryūnosuke zenshū.'],
        [15, 'Wikipedia: The Blair Witch Project', 'the blair witch project', 'https://en.wikipedia.org/wiki/The_Blair_Witch_Project', '| producers      ='],
        [16, 'Wikipedia: Super Slim Me', 'super slim me', 'https://en.wikipedia.org/wiki/Super_Slim_Me', '| director       = Lee Phillips'],
        [17, 'Wikipedia: Paranormal Activity', 'paranormal activity', 'https://en.wikipedia.org/wiki/Paranormal_Activity', '| writer         = Oren Peli'],
        [18, 'Wikipedia: The Mallows', 'the mallows', 'https://en.wikipedia.org/wiki/The_Mallows', '| locmapin = New York#USA'],
        [19, 'Wikipedia: Paranormal Activity 2', 'paranormal activity 2', 'https://en.wikipedia.org/wiki/Paranormal_Activity_2', '| screenplay     ='],
        [20, 'Wikipedia: Paranormal Activity 4', 'paranormal activity 4', 'https://en.wikipedia.org/wiki/Paranormal_Activity_4', '| producer       ='],
        [21, 'Wikipedia: Eraserhead', 'eraserhead', 'https://en.wikipedia.org/wiki/Eraserhead', '| music          ='],
        [22, 'Wikipedia: The Gallows', 'the gallows', 'https://en.wikipedia.org/wiki/The_Gallows', '| producer       ='],
        [23, "Wikipedia: The Tiger: An Old Hunter's Tale", "the tiger: an old hunter's tale", 'https://en.wikipedia.org/wiki/The_Tiger:_An_Old_Hunter%27s_Tale', '| runtime        ='],
        [24, 'Wikipedia: The Shallows', 'the shallows', 'https://en.wikipedia.org/wiki/The_Shallows', 'The Shallows can refer to:'],
        [25, 'Wikipedia: The Prey of the Dragon', 'the prey of the dragon', 'https://en.wikipedia.org/wiki/The_Prey_of_the_Dragon', 'The Prey of the Dragon is a 1921 British silent adventure film directed by F. Martin Thornton and starring Harvey Braban, Gladys Jennings and Hal Martin.'],
        [26, 'Wikipedia: The Sons of the Dragon', 'the sons of the dragon', 'https://en.wikipedia.org/wiki/The_Sons_of_the_Dragon', 'Son of the Dragon}}'],
        [27, "Wikipedia: Whispers: An Elephant's Tale", "whispers: an elephant's tale", 'https://en.wikipedia.org/wiki/Whispers:_An_Elephant%27s_Tale', '| story ='],
        [28, 'Wikipedia: The German Chainsaw Massacre', 'the german chainsaw massacre', 'https://en.wikipedia.org/wiki/The_German_Chainsaw_Massacre', '| writer = Christoph Schlingensief'],
        [29, 'Wikipedia: The Way of the Dog', 'the way of the dog', 'https://en.wikipedia.org/wiki/The_Way_of_the_Dog', '| length = 21 minutes']
    ]

    return pd.DataFrame(values, columns=columns)


@fixture
def rating_data(roi_data, wiki_data):
    '''Output data'''
    values = [
        ['tt1179904', 'https://en.wikipedia.org/wiki/Paranormal_Activity'],
        ['tt0185937', 'https://en.wikipedia.org/wiki/The_Blair_Witch_Project'],
        ['tt5066556', 'https://en.wikipedia.org/wiki/The_Tiger:_An_Old_Hunter%27s_Tale'],
        ['tt0074486', 'https://en.wikipedia.org/wiki/Eraserhead'],
        ['tt0068935', 'https://en.wikipedia.org/wiki/The_Prey_of_the_Dragon'],
        ['tt0069089', 'https://en.wikipedia.org/wiki/Pink_Flamingos'],
        ['tt0390521', 'https://en.wikipedia.org/wiki/Super_Size_Me'],
        ['tt2309260', 'https://en.wikipedia.org/wiki/The_Gallows'],
        ['tt0374102', 'https://en.wikipedia.org/wiki/Open_Water_(film)'],
        ['tt0072271', 'https://en.wikipedia.org/wiki/The_Texas_Chain_Saw_Massacre']
    ]
    glue = pd.DataFrame(values, columns=['imdb_id', 'page_url'])
    return roi_data.merge(glue, 'inner', 'imdb_id').merge(wiki_data, 'inner', 'page_url')


def test_load_imdb(args_data, roi_data):
    '''TEST : load the files and put the info together'''

    output_data = load_imdb(args_data['imdbfile_path']).sort_values(by='roi_ratio', ascending=False)

    assert output_data.imdb_id.values.tolist() == roi_data.imdb_id.values.tolist()
    assert output_data.original_title.values.tolist() == roi_data.original_title.values.tolist()
    assert output_data.roi_ratio.values.tolist() == roi_data.roi_ratio.values.tolist()


def test_load_wiki(args_data, wiki_data):
    '''TEST : load the files and put the info together'''

    output_data = load_wiki(args_data['wikipediafile_path'])

    assert output_data.page_url.values.tolist() == wiki_data.page_url.values.tolist()
    assert output_data.page_title.values.tolist() == wiki_data.page_title.values.tolist()
    assert output_data.page_movie_title.values.tolist() == wiki_data.page_movie_title.values.tolist()


def test_get_movie_rating(args_data, roi_data, wiki_data, rating_data):
    '''TEST : match the movies with the wikipedia pages'''

    output_data = get_movie_rating(args_data['imdbfile_path'], args_data['wikipediafile_path'], args_data['max_movies'], args_data['max_processes'])

    assert_array_equal(output_data.imdb_id.values.tolist(), rating_data.imdb_id.values.tolist())
    assert_array_equal(output_data.original_title.values.tolist(), rating_data.original_title.values.tolist())
    assert_array_equal(output_data.roi_ratio.values.tolist(), rating_data.roi_ratio.values.tolist())
    assert_array_equal(output_data.page_url.values.tolist(), rating_data.page_url.values.tolist())
    assert_array_equal(output_data.page_title.values.tolist(), rating_data.page_title.values.tolist())
    assert_array_equal(output_data.page_movie_title.values.tolist(), rating_data.page_movie_title.values.tolist())


def test_get_movie_rating_multiprocesses(args_data, roi_data, wiki_data, rating_data):
    '''TEST : match the movies with the wikipedia pages'''

    output_data = get_movie_rating(args_data['imdbfile_path'], args_data['wikipediafile_path'], args_data['max_movies'], 4)
    # from IPython import embed; embed()
    # breakpoint()

    # assert output_data[['imdb_id', 'page_url']].equals(rating_data[['imdb_id', 'page_url']])
    assert_array_equal(output_data.imdb_id.values.tolist(), rating_data.imdb_id.values.tolist())
    assert_array_equal(output_data.page_url.values.tolist(), rating_data.page_url.values.tolist())
