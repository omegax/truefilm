from os import path, mkdir, listdir, cpu_count
import pathlib
from typing import Dict, Iterator, Tuple
from json import loads, dump
# from csv import DictReader

from requests import get

# from threadpoolctl import threadpool_limits
from numpy import array, vectorize, argmin, array_split, concatenate, newaxis, take, tile

import pandas as pd

import zipfile
from io import TextIOWrapper
from csv import DictReader

import gzip

from lxml import etree
from lxml.etree import iterparse

from concurrent.futures import ProcessPoolExecutor, Future

from datetime import datetime


def benchmark(func, name: str = None):
    '''
    Usage : benchmark(my_function)()
    '''
    def wrapper(*args, **kwargs):
        func_start = datetime.now()
        result = func(*args, **kwargs)
        func_end = datetime.now()
        time_delta = func_end - func_start
        print('[{0}] [started at {1}] {2}{3}.{4} duration : {5}'.format(func_end, func_start, name + ' - ' if name else '', func.__module__, func.__name__, time_delta))
        return result
    return wrapper


def download_file(url, data_dir:str = path.join(pathlib.Path().absolute(), 'data'), file_name:str = None) -> str:

    if not file_name:
        file_name = url.split('/')[-1]

    file_path = path.join(data_dir, file_name)

    if path.exists(file_path):
        return file_path

    if not path.isdir(data_dir):
        mkdir(data_dir)

    print('Download Starting...')
    raw_data = get(url)

    with open(file_path,'wb') as output_file:
        output_file.write(raw_data.content)

    print('Download Completed\n', file_path)
    return file_path


def read_imdb_file(file_path:str):

    # NOTE : check the zip content `unzip -p data/archive.zip movies_metadata.csv | head -n 2`

    with zipfile.ZipFile(file_path) as zf:
        with TextIOWrapper(zf.open('movies_metadata.csv', 'r'), encoding='utf-8') as metadata_file:
            # Load the movies ratio
            # python 3.9 z = x | y
            for i, m in enumerate(DictReader(metadata_file)):
                yield {
                    **{ k: m[k] for k in {'title', 'original_title', 'imdb_id', 'release_date'} if k in m },
                    **{
                        'movie_id': i,
                        # 'release_date': datetime.strptime(m.get('release_date'), '%Y-%m-%d') if all(x.isdigit() for x in (m.get('release_date') or 'None').split('-')) else None,
                        'release_year': int(m.get('release_date').split('-')[0]) if all(x.isdigit() for x in (m.get('release_date') or 'None').split('-')) else None,
                        'production_companies': ', '.join(x.get('name') for x in eval(m.get('production_companies') or '[]')) \
                            if isinstance(eval(m.get('production_companies') or '[]'), list) \
                            else m.get('production_companies'),
                        'rating': float(m.get('vote_average')) if (m.get('vote_average') or '').replace('.', '', 1).isdigit() else None,
                        'revenue': float(m.get('revenue')) if (m.get('revenue') or '').replace('.', '', 1).isdigit() else None,
                        'budget': float(m.get('budget')) if (m.get('budget') or '').replace('.', '', 1).isdigit() else None,
                        'roi_ratio': float(m.get('revenue')) / float(m.get('budget')) \
                            if (m.get('revenue') or '').replace('.', '', 1).isdigit() and (m.get('budget') or '').replace('.', '', 1).isdigit() and float(m.get('budget')) > 1000 \
                            else None
                    }
                }


def load_imdb(file_path:str) -> pd.DataFrame:

    return pd.DataFrame(read_imdb_file(file_path)) #.set_index('movie_id')


def read_wiki_xml(context: Iterator) -> Iterator:

    for i, (event, element) in enumerate(context):
        title = element.findtext('title')
        yield (
            i,
            title,
            title.lower().removeprefix('wikipedia: ').removesuffix(' (film)'), # '(film series)'
            element.findtext('url'),
            element.findtext('abstract'),
            # str(etree.tostring(element, encoding='utf-8', pretty_print=True), 'utf-8'),
        )

        # Free memory.
        element.clear()
        while element.getprevious() is not None:
            del element.getparent()[0]


def load_wiki(file_path: str) -> pd.DataFrame:

    # NOTE : check the zip content `gzip -cd data/enwiki-latest-abstract.xml.gz | head -n 9`

    fieldnames = ('page_id', 'page_title', 'page_movie_title', 'page_url', 'page_abstract') #, 'page_html'
    with gzip.open(file_path, 'r') as xml_file:

        context = etree.iterparse(xml_file, events=('end',), tag='doc', huge_tree=True)

        wiki_data = pd.DataFrame(read_wiki_xml(context), columns=fieldnames)
        WIKI_TITLES['ids'] = wiki_data.page_id.values
        WIKI_TITLES['values'] = wiki_data.page_movie_title.values # .astype('<U16') # np.dtype('<U16') # np.dtype([('page_movie_title', np.unicode_, 16)])
        return wiki_data

from Levenshtein import distance
# from nltk import edit_distance

# import numpy as np

# from numba import vectorize as nvectorize
# @nvectorize
# def n_argmin(a, axis = 0):
#     return argmin(a)

V_DISTANCE = vectorize(distance)
# V_DISTANCE = vectorize(edit_distance)
WIKI_TITLES = {'ids': None, 'values': None}


def levenshtein_match(values: array) -> array:
    return take(WIKI_TITLES['ids'], argmin(V_DISTANCE(values[:, newaxis], WIKI_TITLES['values']), axis=1))
    # return take(WIKI_TITLES['ids'], n_argmin(V_DISTANCE(values[:, newaxis], WIKI_TITLES['values']), axis=1))


def merge_levenshtein(roi_data: pd.DataFrame, wiki_data: pd.DataFrame, max_processes: int = 1) -> pd.DataFrame:
    max_processes = max(1, cpu_count() - 1 if max_processes == 0 else max_processes)
    if max_processes == 1:
        # from IPython import embed; embed()
        # breakpoint()
        page_ids = levenshtein_match(roi_data.title.str.lower().values)
    else:
        with ProcessPoolExecutor(max_workers = max_processes) as executor:
            page_ids = concatenate(tuple(executor.map(levenshtein_match, array_split(roi_data.title.str.lower().values, roi_data.shape[0]))))

    return roi_data.assign(page_id = page_ids).merge(wiki_data, 'left', 'page_id')


def get_movie_rating(imdbfile_path: str, wikipediafile_path: str, max_movies: int, max_processes: int = 1) -> pd.DataFrame:

    roi_data = benchmark(load_imdb)(imdbfile_path).sort_values(by='roi_ratio', ascending=False)[:max_movies]

    wiki_data = benchmark(load_wiki)(wikipediafile_path)

    return benchmark(merge_levenshtein)(roi_data, wiki_data, max_processes)

import sqlalchemy as db
from sqlalchemy import create_engine, engine as db_engine, MetaData
import sqlite3
import psycopg2


def get_engine(connection = None, prefix: str = 'sqlite://'):
    if not connection and prefix.split(':')[0] == 'sqlite':
        connection = sqlite3.connect(':memory:')
        connection.isolation_level = None

    def get_connection():
        return connection

    return create_engine(prefix, creator = get_connection) #, isolation_level='AUTOCOMMIT')

def get_rating_result(engine: db_engine = None, table_name: str = 'rating_pages'):
    metadata = MetaData()
    table_data = db.Table(table_name, metadata, autoload=True, autoload_with=engine)
    query = db.select([table_data])
    econnection = engine.connect()
    result_proxy = econnection.execute(query)
    result_set = result_proxy.fetchall()
    print(result_set)

def rating_reload(rating_data: pd.DataFrame, engine: db_engine = get_engine()):
    rating_data.set_index('movie_id').to_sql('rating_pages', con=engine, if_exists='replace', index=True, method='multi')
    # print(engine.execute("select * from rating_pages").fetchall())

def rating_insert_db(rating_data: pd.DataFrame, connection):

    fieldnames = [
        'movie_id', 'title', 'original_title', 'imdb_id', 'release_date', 'release_year',
        'production_companies', 'rating', 'revenue', 'budget', 'roi_ratio',
        'page_id', 'page_title', 'page_movie_title', 'page_url', 'page_abstract',
    ]

    cursor = connection.cursor()

    cursor.execute('''
create table if not exists rating_pages(
movie_id integer primary key, title string, original_title string, imdb_id string, release_date string, release_year integer,
production_companies string, rating float, revenue float, budget float, roi_ratio float,
page_id integer, page_title string, page_movie_title string, page_url string, page_abstract string
)''')

    cursor.execute('delete from rating_pages')

    cursor.executemany(
        f"insert into rating_pages({', '.join(fieldnames)}) values ({', '.join(['?'] * len(fieldnames))})",
        rating_data[fieldnames].values
    )
    cursor.close()

import monetdbe

def rating_monetdb_db(rating_data: pd.DataFrame, connection: monetdbe.Connection):

    cursor = connection.cursor()
    cursor.execute('drop table if exists rating_pages')
    cursor.create('rating_pages', rating_data)
    cursor.close()
    # print('\nMonetDBe\n', connection.execute('select * from rating_pages').fetchdf())

def get_connection(database: str):

    if database.lower() == 'sqlite':
        connection = sqlite3.connect(':memory:', isolation_level = None)
        engine = get_engine(connection, 'sqlite://')
        return connection

    elif database.lower() == 'postgresql':
        connection = psycopg2.connect("user=postgres", isolation_level = None)
        engine = get_engine(connection, 'postgresql://postgres')
        return connection

    elif database.lower() == 'monetdb':
        connection = monetdbe.connect(':memory:')
        connection.isolation_level = None
        return connection
    else:
        return None

def store_rating(rating_data: pd.DataFrame, connection, database: str):

    if connection and database.lower() == 'sqlite':
        engine = get_engine(connection, 'sqlite://')

        benchmark(rating_reload, 'SQLite')(rating_data, engine)

    elif connection and database.lower() == 'postgresql':
        engine = get_engine(connection, 'postgresql://postgres')

        benchmark(rating_reload, 'PostgreSQL')(rating_data, engine)
        engine.execute('drop table if exists rating_pages')

    elif connection and database.lower() == 'monetdb':
        benchmark(rating_monetdb_db, 'MonetDB')(rating_data, connection)
