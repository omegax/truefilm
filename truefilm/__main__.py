# __main__.py
from sys import argv
import argparse

from truefilm.rating import (
    path,
    pathlib,
    psycopg2,
    sqlite3,
    monetdbe,
    download_file,
    get_movie_rating,
    get_connection,
    store_rating,
)

def get_args():

    parser = argparse.ArgumentParser(
        prog='python truefilm',
        description='Provide the ratio of budget to revenue for movie in the IMDB dataset along with the corresponding Wipedia page'
    )

    parser.add_argument('-i', '--imdb-url', type=str, default='https://www.kaggle.com/rounakbanik/the-movies-dataset/download', help='Valid URL for the IMDB dataset')
    parser.add_argument('-w', '--wikipedia-url', type=str, default='https://dumps.wikimedia.org/enwiki/latest/enwiki-latest-abstract.xml.gz', help='Valid URL for the Wikipedia dump dataset')
    parser.add_argument('-n', '--max-movies', type=int, default=10, help='Max rating movie count')
    parser.add_argument('-p', '--max-processes', type=int, default=1, help='Max processes workers')
    parser.add_argument('-s', '--source-dir', type=str, default=pathlib.Path().absolute(), help='Data source directory')
    parser.add_argument('-d', '--database', type=str, default='sqlite', help='Target Database')
    return parser.parse_args()


def main(args):

    data_dir = path.join(args.source_dir, 'data')
    if args.imdb_url:
        imdbfile_path = download_file(args.imdb_url, data_dir, 'archive.zip')
    if args.wikipedia_url:
        wikipediafile_path = download_file(args.wikipedia_url, data_dir)

    movie_rating = get_movie_rating(imdbfile_path, wikipediafile_path, args.max_movies, args.max_processes)
    print(movie_rating[-11:][['title', 'page_title', 'revenue', 'budget', 'roi_ratio', 'rating']])

    connection = get_connection(args.database)
    store_rating(movie_rating, connection, args.database)
    if connection:
        connection.close()

    return movie_rating

from test import build_test_data

def init_test(args):

    data_dir = path.join(args.source_dir, 'data')
    if args.imdb_url:
        imdbfile_path = download_file(args.imdb_url, data_dir, 'archive.zip')
    if args.wikipedia_url:
        wikipediafile_path = download_file(args.wikipedia_url, data_dir)

    build_test_data(imdbfile_path, wikipediafile_path, args.source_dir)


if __name__ == '__main__':

    args = get_args()

    main(args)
    # init_test(args)

    exit(0)
