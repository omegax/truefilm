from sys import path as sys_path
from os import path

# The directory containing this file
HERE = path.abspath(path.dirname(__file__))
if HERE not in sys_path:
    sys_path.insert(0, HERE)

# Version of the package
__version__ = '1.0.0'
